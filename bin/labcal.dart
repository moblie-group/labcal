import 'dart:io';
import 'dart:math';

import 'package:labcal/labcal.dart' as labcal;

void main(List<String> arguments) {
  print("Please input your number: ");
  String list = (stdin.readLineSync()!);

  print(Calculator(list).result());

}

class Calculator{
    List token = [];
    List postfix = [];
    num eva = 0;
 Calculator(String x){
  token = tokenizing(x);
  postfix = infixToPostfix(token);
  eva = EvaluatePostfix(postfix); 
 }
 num result(){
  return eva;
 }
}



List tokenizing(String input) {
  List list = [];
  String empt = "";
  for (int i = 0; i < input.length; i++) {
    if (input[i] == " ") {
      if (empt.isNotEmpty) {
        list.add(empt);
        empt = "";
      }
    } else {
      empt += input[i];
    }
  }
  if (!input[input.length - 1].contains(" ")) {
    list.add(empt);
  }
  return list;
}

List infixToPostfix(List infix) {
  List operators = [];
  List postfix = [];

  for (int i = 0; i < infix.length; i++) {
    if (infix[i] == '(') {
      operators.add(infix[i]);
    }
    if (infix[i] == ')') {
      while (operators.last != '(') {
        postfix.add(operators.last);
        operators.removeLast();
      }
      operators.remove("(");
    }
    if (infix[i] == "+" ||
        infix[i] == "-" ||
        infix[i] == "*" ||
        infix[i] == "/" ||
        infix[i] == "^") {
      while (operators.isNotEmpty &&
          operators.last != '(' &&
          precedence(infix[i]) <= precedence(operators.last)) {
        postfix.add(operators.last);
        operators.removeLast();
      }
      operators.add(infix[i]);
    }
    if (int.tryParse(infix[i]) != null) {
      postfix.add(infix[i]);
    }
  }
  while (operators.isNotEmpty) {
    postfix.add(operators.last);
    operators.removeLast();
  }
  return postfix;
}

int precedence(var o) {
  return (o == '+' || o == '-')
      ? 1
      : (o == '*' || o == '/')
          ? 2
          :(o == '^' )? 3 : 0 ;
}

num EvaluatePostfix(List inputt) {
  List val = [];
  num r = 0;
  num l = 0;

  for (int i = 0; i < inputt.length; i++) {
    if (double.tryParse(inputt[i]) != null) {
      val.add(double.parse(inputt[i]));
    } else {
      r = val.last;
      val.removeLast();
      l = val.last;
      val.removeLast();
      if (inputt[i] == '+') {
        val.add(l + r);
      } else if (inputt[i] == '-') {
        val.add(l - r);
      } else if (inputt[i] == '*') {
        val.add(l * r);
      } else if (inputt[i] == '/') {
        val.add(l / r);
      } else if (inputt[i] == '^') {
        val.add(pow(l, r));
      }
    }
  }
  return val.first;
}

